﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ambience_in_out : MonoBehaviour
{
    AudioSource audiosource1;
    AudioSource audiosource2;
    public AudioClip outdoorClip;
    public AudioClip indoorClip;
    [Range(0f, 1f)] public float Audioclip1Volume;
    [Range(0f, 1f)] public float Audioclip2Volume;
    public float fade = 3f;

    // Start is called before the first frame update
    void Start()
    {
        audiosource1 = gameObject.AddComponent<AudioSource>();
        audiosource1.clip = outdoorClip;
        audiosource1.loop = true;
        audiosource1.volume = 0f;
        audiosource1.playOnAwake = true;
        audiosource1.Play();

        audiosource2 = gameObject.AddComponent<AudioSource>();
        audiosource2.clip = indoorClip;
        audiosource2.loop = true;
        audiosource2.volume = 0f;
        audiosource2.playOnAwake = false;
        audiosource2.Play();


        StartCoroutine(Fadein(audiosource1, Audioclip1Volume));
    }

    IEnumerator Fadein(AudioSource source, float volume) // Фукнция Fadein для переменных нужного аудиосорса и нужной громкости
    {

        while (source.volume < volume) // Пока громкость сорса не достигнет громкости, которая передается из переменной AudioClip1Volume или AudioClip2Volume
        {
            source.volume += (Time.deltaTime / fade); // Постепенно добавляем громкость
            yield return null;
        }
    }
    IEnumerator Fadeout(AudioSource source) // Фукнция Fadeout для переменных нужного аудиосорса и нужной громкости
    {

        while (source.volume > 0f)// Пока громкость сорса больше нуля, будем постепенно уменьшать громкость
        {
            source.volume -= (Time.deltaTime / fade);
            yield return null;
        }

    }

    private void OnTriggerEnter(Collider other) // Функция запускается, если что-то вошло в коллайдер
    {
        if (other.tag == "Player") // Проверяем условие если коолайдер пересек объект с тегом Player
        {

            StartCoroutine(Fadeout(audiosource1));  // Делаем фейдаут для звука улицы
            StartCoroutine(Fadein(audiosource2, Audioclip2Volume)); // Делаем фейдин для звука дома

        }
    }

    private void OnTriggerExit(Collider other) // Функция запускается, если что-то вышло из коллайдера
    {
        if (other.tag == "Player") // Проверяем условие если коолайдер пересек объект с тегом Player
        {

            StartCoroutine(Fadein(audiosource1, Audioclip1Volume)); // Делаем фейдин для звука улицы
            StartCoroutine(Fadeout(audiosource2)); // Делаем фейдаут для звука дома

        }

    }
}


