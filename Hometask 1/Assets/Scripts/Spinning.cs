﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinning : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(0f, 0f, 100 * Time.deltaTime, Space.Self); // Вращает объект каждый фрейм на 100 единиц по оси Y
    }
}
