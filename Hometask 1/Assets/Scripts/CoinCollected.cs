﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollected : MonoBehaviour
{

    private AudioSource _source;
    public AudioClip _clip;
    public GameObject Coin;
    public bool collided = false;
    
    // Start is called before the first frame update
    void Start()
    {
        _source = gameObject.AddComponent<AudioSource>();

    }


    private void OnTriggerEnter(Collider other)
    {
        if (!collided);
        {
            if (other.tag == "Player")
            {
                _source.clip = _clip;
                _source.loop = false;
                _source.volume = 1f;
                _source.Play();

                Destroy(Coin);
                collided = true;
               
            }
           
        }
        

    }
 
}
