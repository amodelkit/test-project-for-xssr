﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueScript : MonoBehaviour
{

    public AudioClip[] _clip;
    public AudioSource _source;
    int SampleIndex;


    // Start is called before the first frame update
    void Start()
    {
        _source = gameObject.AddComponent<AudioSource>();
       
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        SampleIndex = Random.Range(0, 4);
        _source.clip = _clip[SampleIndex];
        _source.volume = 0.5f;
        _source.Play();
    }
}
