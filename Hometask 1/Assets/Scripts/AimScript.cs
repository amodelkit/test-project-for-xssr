﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimScript : MonoBehaviour
{
    public AudioSource _source;
    public AudioClip music;

    // Start is called before the first frame update
    void Start()
    {
        _source = gameObject.AddComponent<AudioSource>();
        _source.playOnAwake = false;

    }



    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") ;
        _source.playOnAwake = false;
        _source.clip = music;
        _source.loop = false;
        _source.volume = 0.5f;
        _source.Play();
    }
}








