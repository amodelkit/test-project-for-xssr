﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Out_Amb : MonoBehaviour
{
    public AudioSource AmbientSource;
    public AudioClip AmbientClip;
   

    void Start()
    {
        AmbientSource = gameObject.GetComponent<AudioSource>();
        AmbientSource.clip = AmbientClip;
        AmbientSource.loop = true;
        AmbientSource.Play();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
