﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeAmbience : MonoBehaviour
{
    public AudioClip[] Tree_clip;
    AudioSource Tree_source;
    void Start()
    {
        Tree_source = GetComponent<AudioSource>();
        Tree_source.clip = Tree_clip[Random.Range(0, Tree_clip.Length)];
        Tree_source.PlayDelayed(Random.Range(0.2f, 1f));
    }
}