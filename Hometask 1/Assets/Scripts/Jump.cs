﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController; // строка неймспейса для работы с контроллером Инвектора

public class Jump : MonoBehaviour
{
    AudioSource audiosource;
    public AudioClip[] jumpclips; // Массив для сэмплов бега
    vThirdPersonInput tpInput; // Переменная для  обращения к контроллеру персонажа и для того, чтобы проверять магнитуду. Таким образом шаги не будут звучать, когда мы стоим на месте
    vThirdPersonController tpController; // Переменная для обращения к компоненту контроллера. Для проверки стейта спринта
    public float VolumeMin = 1f; // Минимальная громкость шагов для рандома громкости
    public float pitchMin = 1f; // Минимальное значение питча для рандома
    public float pitchMax = 1f; // Максимальное значение питча для рандома
    int lastIndex; // переменная для проверки какой был прошлый индекс сэмпла, чтобы не повторялся один и тот же сэмпл
    int newIndex; // переменная для запоминания нового сформированного индекса сэмпла

    void Start()
    {
        audiosource = GetComponent<AudioSource>(); // Говорим, что переменной audiosource присваевается путь к компоненту аудиосорс, который висит на компоненте (вешаем скрипт на игрока)
        tpInput = GetComponent<vThirdPersonInput>(); // обращаемся к компоненту инпута
        tpController = GetComponent<vThirdPersonController>(); // обращаемся к контроллеру
    }


    void jump () // функция проигрывания шагов, которая вызывается из анимации
    {
      
        {
            Randomization(jumpclips.Length); // запускаем функцию рандомизации номера клипа Randomization
            audiosource.volume = Random.Range(VolumeMin, 0.2f); // выставляем случайное значние громкости от минимума для максимум 1f 
            audiosource.pitch = Random.Range(pitchMin, pitchMax); // выставляем случайный питч
            audiosource.PlayOneShot(jumpclips[newIndex]); // воспроизводим сэмпл случайного индекса
            lastIndex = newIndex;

        
        
        }

    }

    void Randomization(int cliplenght) // созданная нами функция для выбора случайного значения индекса сэмпла. в эту функицию мы передаем число сколько всего у нас сэмплов в массиве
    {
        newIndex = Random.Range(0, cliplenght); // присваеваем переменной newIndex случайное число от 0 до числа переданноего в функцию (а мы передаем в нее количество сэмплов в массиве)
        while (newIndex == lastIndex) // запускаем цикл, если новый индекс будет равен такому же числу как уже было = если сэмпл будет такой же, то
            newIndex = Random.Range(0, cliplenght); // снова запускаем рандом, пока не получим новое число для индекса
    }
}